# CLIR-ext4 project
# Written by nqngo

"""
Contain the Information Retrieval framework
using Lucene.
"""

import lucene, os, codecs
from string import Template

class IR(object):
    """
    This is a the Lucene IR engine class.
    """
    def __init__(self, params):
        """
    Initialise Lucene IR engine. If there has yet to be an index directory,
    the class will start index the documents in the given the data directory
    and downward recursively.
        
    Parameters:
    params -- PyLucene settings path to the document sets.
        """
        lucene.initVM(lucene.CLASSPATH)
        self.params = params
        self.version = lucene.Version.LUCENE_CURRENT
        self.analyzer = lucene.StandardAnalyzer(self.version)
        
        if not os.path.exists(self.params['indexDir']):
            self.newIndex()
        
        fsDir = lucene.SimpleFSDirectory(lucene.File(self.params['indexDir']))
        self.seeker = lucene.IndexSearcher(fsDir, True)
        self.newParser()
    
    
    def update(self):
        """
    Do updating rule.    
        """
        self.newParser()

    
    def newParser(self):
        """
    Create a new parser for a given field and set Default Operator.
        """
        self.parser = lucene.QueryParser(self.version,
                                         self.params['parserField'],
                                         self.analyzer)
        self.parser.setDefaultOperator(lucene.QueryParser.Operator.OR)
    
        
    def query(self, raw_query, maxDocReturn):
        """
    Search for the new query.
    
    Paramters:
    raw_query -- the input query.
    maxDocReturn -- the maximum number of documents returned.
    """
        if self.parser is None:
            print "ERROR: No parser specified."
            exit(1)
        
        query = self.parser.parse(raw_query)
        return self.seeker.search(query, int(maxDocReturn))
    
    
    def newIndex(self):
        """
    Index the documents from the given root path and store the result in
    the index dir.
        """
        print 'Indexing...'
        
        if not os.path.exists(self.params['indexDir']):
            os.mkdir(self.params['indexDir'])
        index = lucene.SimpleFSDirectory(lucene.File(self.params['indexDir']))
        writer = lucene.IndexWriter(index, self.analyzer, True,
                        lucene.IndexWriter.MaxFieldLength.LIMITED)
        writer.MaxFieldLength(int(self.params['maxFieldLength']))
        
        self.indexDocs(self.params['dataDir'], writer)
        writer.optimize()
        writer.close()
        
        print 'completed.'

        
    def indexDocs(self, root, writer):
        """
    Index all documents in given root path.
        """
        for root, dirnames, filenames in os.walk(root):
            for filename in filenames:
                # Index only the given extension
                if not filename.endswith(self.params['docExtension']):
                    continue
                try:
                    path = os.path.join(root, filename)
                    f = codecs.open(path, 'r', encoding = 'utf-8')
                    contents = f.read()
                    f.close()
                        
                    # Catalog the document
                    doc = lucene.Document()
                    doc.add(lucene.Field("name", filename,
                                  lucene.Field.Store.YES,
                                  lucene.Field.Index.NOT_ANALYZED))
                    doc.add(lucene.Field("path", path,
                                  lucene.Field.Store.YES,
                                  lucene.Field.Index.NOT_ANALYZED))
                    doc.add(lucene.Field("contents", contents,
                                  lucene.Field.Store.NO,
                                  lucene.Field.Index.ANALYZED))
                    
                    writer.addDocument(doc)
                    
                except Exception, e:
                    print "Failed to index:", e
