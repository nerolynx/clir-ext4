# CLIR-ext4 project
# Written by nqngo
"""
Ultility scripts containing all query augmentation functions.
"""
import re


def augGIZA(query, giza, mode):
    """
    Query augmentation using GIZA++ word thesaurus. The query
    will simply augment the query with related words in other
    language.
    
    Parameters:
    query -- the raw query.
    giza -- the GIZA object.
    mode -- optional setting for running mode. Maybe set to:
             all -- augment all definition of the word
             n(int) -- take n max number of definitions
    """
    # Safety checking
    if not query:
        return query
        
    tokens = query.split('\ ')
    aug = []
    for token in tokens:
        for language in giza.thesaurus:
            candidate = language.get(token.strip("\'.?!:\n"), None)
            if candidate is None:
                continue
            if mode in 'all':
                for definition in candidate:
                    aug.append(definition)
            elif mode.isdigit():
                for definition in candidate[:int(mode)]:
                    aug.append(definition)
            aug.append(candidate[0])
    
    # If empty, return the query only
    if not aug:
        return ' '.join(tokens)
    return ' '.join([' '.join(tokens), ' '.join(set(aug))])
    

def format(text):
    """
    Format the text back to Lucene standard.
    """
    tokens = { "\ ": " ",
             }
    for token in tokens:
        text = text.replace(token, tokens[token])
    return text