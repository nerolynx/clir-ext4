# CLIR-ext4 project
# Written by nqngo

"""
Command line interface to the Cross Language Interface
using PyLucene.
"""

import re

class interfaceCLI(object):
    """
    The CLI interface to the CLIR module.
    """
    def __init__(self):
        """Welcome message."""
        print """University of Melbourne: 
Cross Language Information Retrieval System Prototype.
    CLIR-ext4 (2012) written by btawfic, ankita, nqngo.
===================================================================
Type in the desired query. Read the accompanied README before using
the software, for shell help please type :help\n"""
    
    def getInput(self, params):
        """Get and process the input."""
        buf = raw_input(">")
        # Empty
        if not buf:
            return ['E']
        if ":quit" in buf:
            self.cmdQuit()
        if ":help" in buf:
            return self.cmdHelp()
        if ":set" in buf:
            return self.cmdSet(buf, params)
        if ":env" in buf:
            return self.cmdEnv(buf, params)
        if ":index" in buf:
            return self.cmdIndex()
        if ":giza" in buf:
            return self.cmdGiza()
        return ['Q', buf]


    def cmdHelp(self):
        """Print the help instruction."""
        print """Program commands:
        :help -- show commands.
        :quit -- exit the program.
        :set <module>.<param> <setting> -- modify the system parameters.
        :env <module> -- display the module configuration. If no module
                         is specified, display all module.
        :index -- force reindex the docset.
        :giza -- force rebuilding giza vocabulary.
    
        Syntax rules:
        Please reference to Lucene syntax.
        \n"""
        return ['H']


    def cmdQuit(self):
        """Quit the program."""
        print """Exit..."""
        exit()

    def cmdGiza(self):
        """Force rerun giza."""
        print "Re-running GIZA++."
        return ['G']


    def cmdSet(self,inline, params):
        """Set the system variables."""
        tmp = inline.rsplit(':set')[1].strip(' \n').split(' ')
        # Bound the attribute / value syntax
        if len(tmp) > 1:
            tag, value = tmp[0], ' '.join(tmp[1:])
        else:
            print "Error: Usage :set <module>.<attribute> <value>"
            return ['E']
        tmp = tag.split('.')
        # Bound the module /attribute syntax
        if len(tmp) > 1:
            module, attr = tmp[0], '.'.join(tmp[1:])
        else:
            print "Error: Usage :set <module>.<attribute> <value>"
            return ['E']
        # Safety checking
        if module not in params:
            print 'Error: No %s module' % module
            return ['E']
        if attr not in params[module]:
            print 'Error: No %s attribute in %s.' % (attr, module)
            return ['E']
        params[module][attr] = value
        print "%s.%s set to %s" % (module, attr, value)
        return ['S',module,attr]


    def cmdEnv(self,inline, params):
        """Display the system configuration."""
        module = inline.rsplit(':env')[1].strip(' \n')
        if not module:
            for module in params:
                self.showModule(module, params)
            return ['E']
        if module not in params:
            print 'Error: No %s module' % module
            return ['E']
        self.showModule(module, params)
        return ['V']


    def cmdIndex(self):
        """Display reindex message and send instruction to main."""
        print "Starting reindex."
        return ['I']
        

    def showModule(self,module, params):
        """Display all configuration attribute of a module."""
        print '[%s]' % module
        for key in params[module]:
            print '%s = %s' % (key, params[module][key])
