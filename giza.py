# CLIR-ext4 project
# Written by ankita, nqngo

"""
Module for handling Giza-PP.
Make sure to read the accompanied README file to
avoid Executable Error!
"""

import subprocess, os, codecs, shutil
from collections import defaultdict

class GIZA(object):
    """
    Class for running giza-pp.
    """
    def __init__(self, params):
        """Initialise and parse the parameters."""
        self.params = params
        self.extractDict()
    
    def update(self):
        """Do update actions."""
        # Nothing to do yet
        return
    
    
    def run(self):
        """
        This function run giza++ and generate all requested file.
        """
        # Preprocessing
        if not os.path.exists(self.params['gizaDir']):
            os.mkdir(self.params['gizaDir'])
        if not os.path.exists(self.params['gizaOut']):
                os.mkdir(self.params['gizaOut'])
        srcCorpus = os.path.join(self.params['corpusDir'], self.params['src'])
        
        trgCorpura = [(os.path.join(self.params['corpusDir'],
                                    trg.replace(' ', '')),
                       trg.replace(' ',''))
                       for trg in self.params['trg'].split(',')]
        self.vdict = []
        self.thesaurus = []
        # Create a vocabulary and sentence translation index.
        for trgCorpus in trgCorpura:
            subprocess.call([self.params['plain2snt'],
                            srcCorpus,
                            trgCorpus[0]])
                            
        # Create word classes files for the language pairs. (Higher IBM model)
        subprocess.call([self.params['mkcls'],
                        "-p" + srcCorpus,
                        "-V" + srcCorpus + ".vcb.classes",
                        ])
        for trgCorpus in trgCorpura:
            subprocess.call([self.params['mkcls'],
                            "-p" + trgCorpus[0],
                            "-V" + trgCorpus[0] + ".vcb.classes",
                            ])
            
        # Create upstream + downstream cooccurence files.
        
            cost = open(srcCorpus + "_" + trgCorpus[1] + ".cooc", "wb")
            subprocess.call([self.params['snt2cooc'],
                        srcCorpus + ".vcb",
                        trgCorpus[0] + ".vcb",
                        srcCorpus + "_" + trg + ".snt"],
                        stdout = cost)
            cost.close()
            cots = open(trgCorpus[0] + "_" + self.params['src'] + ".cooc", "wb")
            subprocess.call([self.params['snt2cooc'],
                        trgCorpus[0] + ".vcb",
                        srcCorpus + ".vcb",
                        trgCorpus[0] + "_" + self.params['src'] + ".snt"],
                        stdout = cots)
            cots.close()
            cwd = os.getcwd()
            os.chdir(os.path.join(cwd, self.params["gizaDir"]))
            # Clean the giza temporary folder
            for f in os.listdir('.'):
                os.unlink(f)
            
            # Run GIZA
            proc = subprocess.Popen([os.path.join(cwd, self.params["giza"]),
                "-S", os.path.join(cwd, srcCorpus) + ".vcb",
                "-T", os.path.join(cwd, trgCorpus[0]) + ".vcb",
                "-C",
                os.path.join(cwd, srcCorpus + "_" + trgCorpus[1] + ".snt"),
                "-CoocurrenceFile",
                os.path.join(cwd, srcCorpus + "_" + trgCorpus[1] + ".cooc")])
            # Prevent race condtions in multiple GIZA++ instances
            while proc.poll() is None:
                continue
            
            os.chdir(cwd)
            # Move neccessary file into giza output folder
            trgName = os.path.join(self.params['gizaOut'],
                        self.params['src'] + "_" + trgCorpus[1])
            
            for f in os.listdir(self.params['gizaDir']):
                if f.endswith('.t3.final'):
                    trg = trgName + ".final"
                elif f.endswith('.trn.src.vcb'):
                    trg = trgName + ".src"
                elif f.endswith('.trn.trg.vcb'):
                    trg = trgName + ".trg"
                else:
                    continue
                src = os.path.join(self.params['gizaDir'], f)
                shutil.move(src, trg)
            self.vdict.append(namePath)
        # Extract the dictionary and move everything in
        self.extractDict()
        
    def extractDict(self):
        """
        Return an extracted vocabulary from the source and target
        vocabulary pairs from GIZA++.
        """
        # Generate the filename
        data = [(f, f.split('.')[0] + '.src', f.split('.')[0] + ".trg")
                    for f in os.listdir(self.params['gizaOut'])
                    if '.final' in f]
        
        self.thesaurus = []
        for language in data:
            # Build unique-word lookup table
            src = self.catalogVcb(os.path.join(self.params['gizaOut'],
                                  language[1]))
            trg = self.catalogVcb(os.path.join(self.params['gizaOut'],
                                  language[2]))
            f = open(os.path.join(self.params['gizaOut'], language[0]), 'r')
            thesaurus = defaultdict(list)
            # Process line one by one
            line = f.readline()
            while line:
                data = line.strip('\n').split(' ')
                #print data[0], data[1]
                if data[0] in src:
                    thesaurus[src[data[0]]].append(trg[data[1]])
                line = f.readline()
            f.close()
            self.thesaurus.append(thesaurus)


    def catalogVcb(self, file):
        """
        Read the input file and extract the unique word id.
    
        Parameter:
        file -- the input file.
        """
        f = codecs.open(file, 'r', encoding='utf-8')
        vocab = {}
        # Process the line one by one
        line = f.readline()
        while line:
            data = line.strip('\n').split(' ')
            vocab[str(data[0])]= data[1]
            line = f.readline()
        f.close()
        return vocab

"""
DEBUG = True

if DEBUG:
    params = {
    'plain2snt': "bin/plain2snt",
    'corpusDir': "corpus",
    'src': 'en',
    'trg': 'de, fr',
    'mkcls': "bin/mkcls",
    'snt2cooc': "bin/snt2cooc",
    'gizaDir': 'tmp',
    'gizaOut': 'giza',
    'giza': 'bin/GIZA++',
    }
    g = GIZA(params)
    # g.run()
"""