# CLIR-ext4 project
# Written by nqngo
"""
The main program for CLIR-ext4 project.
"""
import irlucene, interface, giza
import queryaug, re

def parseConfig(file):
    """Parse the configuration from the ini file."""
    f = open(file, 'rb')
    raw = f.readlines()
    f.close()
    
    ignoreTokens = ['#','\n','\r']
    params = {}
    
    for line in raw:
        # Sanitise the line
        line = line.lstrip(' ')
        if line[0] in ignoreTokens:
            continue
        if line[0] == '[':
            heading = line.strip('[]\n')
            params[heading] = {}
            current = params[heading]
        else:
            setting = [side.strip(' \n') for side in line.split('=')]
            current[setting[0]] = setting[1]
    return params


def main():
    """The main function."""
    # Default config file
    params = parseConfig('settings.ini')
    irsys = irlucene.IR(params['LUCENE'])
    gpp = giza.GIZA(params['GIZA'])
    smt = moses.MOSES(params['MOSES'])
    cli = interface.interfaceCLI()
    # Main loop
    while True:
        query = cli.getInput(params)
        # Debugging response token
        if params['MAIN']['debug'] in 'True':
            print query
        # Handle input responses
        if query[0] is 'I':
            irsys.newIndex()
        if query[0] is 'G':
            gpp.run()
        if query[0] is 'S':
            irsys.update()
            gpp.update()
        if query[0] is 'Q':
            if params['QUERY']['augmentMode'] in "GIZA":
                mod_query = queryaug.augGIZA(re.escape(query[1]),
                                gpp,
                                params['QUERY']['modeGIZA'])
            else:
                mod_query = re.escape(query[1])
            # Reformat to Lucene standard
            mod_query = queryaug.format(mod_query)
            # Debug
            if params['MAIN']['debug'] in 'True':
                print mod_query
            # Display result
            display(irsys.query(mod_query,
                                params['QUERY']['maxDocReturn']),
                                irsys)


def display(searchResult, ir):
    """Dummy display codes. User is to customise the script here."""
    for scoreDoc in searchResult.scoreDocs:
        doc = ir.seeker.doc(scoreDoc.doc)
        print 'name: %s, score: %.5f, path: %s' % (doc.get("name"),
                                                 scoreDoc.score,
                                                doc.get("name"))


if __name__ == '__main__':
    main()