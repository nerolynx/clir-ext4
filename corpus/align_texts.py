# clir-ext4 script
# written by btawfic

import codecs

en_deFile = "europarl_en_de"
en_frFile = "europarl_en_fr"
deFile = "europarl_de"
frFile = "europarl_fr"

en_de_doc = codecs.open(en_deFile, "r", encoding='utf-8').readlines()
en_fr_doc = codecs.open(en_frFile, "r", encoding='utf-8').readlines()
de = codecs.open(deFile, "r", encoding='utf-8').readlines()
fr = codecs.open(frFile, "r", encoding='utf-8').readlines()

en_de = zip(en_de_doc, de)
en_fr = zip(en_fr_doc, fr)

de_pair = {}
fr_pair = {}

for pair in en_de:
	de_pair[pair[0]] = pair[1]
for pair in en_fr:
	fr_pair[pair[0]] = pair[1]

out = []

for key in de_pair:
	if key in fr_pair:
		out.append((key, de_pair[key], fr_pair[key]))

en_out = codecs.open("en", "w", encoding='utf-8')
de_out = codecs.open("de", "w", encoding='utf-8')
fr_out = codecs.open("fr", "w", encoding='utf-8')

en_out.write(''.join(e[0] for e in out))
de_out.write(''.join(e[1] for e in out))
fr_out.write(''.join(e[2] for e in out))