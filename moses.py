# CLIR-ext4 project
# Written by btawfic

"""
Module for handling Moses decoder
NOTE: THIS MODULE IS NOT COMPLETE DUE TO US UNABLE TO COMPILE
AND RUN MOSES BINARY. THE CODE WRITTEN BELOW IS TO GIVE FUTURE
DEVELOPERS AN IDEA OF HOW TO EXTEND THE CURRENT FRAMEWORK AND
ADD MORE DATA.
FOR MORE INFORMATION GO TO:
http://www.statmt.org/moses/?n=Moses.Baseline
"""

import subprocess, os, codecs
from collections import defaultdict

class Moses(object):
	"""Class for running Moses decoder"""
	def __init__(self, params):
		self.params = params

	def update(self):
		"""Do update actions"""
		return

	def run(self):
		"""
		This functions runs the Moses decoder and returns the
		input translated into a given language.
		"""
		# Check to ensure that the output directory has been created
		if not os.path.exists(self.params['mosesOutDir']):
			os.mkdir(self.params['mosesOutDir'])

		"""
		Get the source and target languages as well as the
		source/target language sentance aligned documents for 
		training
		"""
		# srcLang = self.params['srcLang']
		# srcLangDoc = open(self.params['srcLangDoc'], 'r')
		# tarLang = self.params['tarLang']
		# tarLangDoc = open(self.params['tarLangDoc'], 'r')

		""" Prepare the data for the translation system """
		# srcLangTok = open(self.params['mosesOutDir'] + ".tok" + srcLang)
		# tarLangTok = open(self.params['mosesOutDir'] + ".tok" + tarLang)
		# tokScript = os.path.join([self.params['mosesScripts'],
		# 						"tokenizer/tokenizer.perl")
		# subprocess.call(tokScript
		# 				"-l", srcLang,
		# 				stdin = srcLangDoc,
		# 				stdout = srcLangTok
		# 				])
		
		""" Train the truecaser here """
		# trainTruecaserScript = os.path.join([self.params['mosesScripts'],
		# 						"recaser/train-truecaser.perl")
		# truecaseSrc = open('truecase-model' + srcLang, 'wb')
		# truecaseTar = open('truecase-model' + tarLang, 'wb')
		# subprocess.call([trainTruecaserScript,
		# 				"--model", truecaseSrc
		# 				"--corpus", srcLangTok
		# 				])
		# subprocess.call([trainTruecaserScript,
		# 		"--model", truecaseTar
		# 		"--corpus", tarLangTok
		# 		])

		""" Truecase the files here """
		# srcLangTrue = open(srcLangDoc + ".true" + srcLang, 'wb')
		# tarLangTrue = open(srcLangDoc + ".true" + tarLang, 'wb')
		# truecaseScript = os.path.join([self.params['mosesScripts'],
		# 						"recaser/truecase.perl")
		# subprocess.call([truecaseScript,
		# 				"--model", truecaseSrc, 
		# 				stdin = srcLangDoc,
		# 				stdout = srcLangTrue)		
		# subprocess.call([truecaseScript,
		# 				"--model", truecaseTar, 
		# 				stdin = tarLangDoc,
		# 				stdout = tarLangTrue)

		""" 
		Language model training should be done here
		For information how to train the language model with options
		view the IRSTLM documentation here:
		http://sourceforge.net/apps/mediawiki/irstlm/index.php?title=Main_Page
		And for an example of how the language model can be trained to create
		a 3-gram langauge model, removing singletons, smoothing with improved
		Kneser-Ney and adding sentence boundary symbols view the 
		"Language Model Training" Section of:
		http://www.statmt.org/moses/?n=Moses.Baseline
		"""

		"""
		Training the Translation system is the next step and this 
		relies on GIZA++. An example use of the GIZA++ in training
		the translation system can be found in the "Training the 
		Translation System" Section of:
		http://www.statmt.org/moses/?n=Moses.Baseline
		"""

		"""
		An option for tuning the system can be added here. This was not
		going to be implemented as part of the original project because
		although it would make the system more accurate, it would take
		a very long time to run. 
		More details can be found in the "Tuning" Section of:
		http://www.statmt.org/moses/?n=Moses.Baseline
		"""

		"""
		Moses should be invoked here to provide a translation for a given
		input phrase, or file.
		The format in which this is done on the command line is as follows:
		INPUTPHRASE | mosesDir/bin/moses -f outputFromTraining/model/moses.ini

		